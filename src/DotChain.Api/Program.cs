using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DotChain;
using DotChain.Api.Models.Wallets;
using Elastic.Apm.AspNetCore;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Interfaces;
using Microsoft.OpenApi.Models;
using Raven.Client;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);
ConfigureLogging();
builder.Host.UseSerilog();

#region Dependency Injection

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "DotChain",
        Version = "v1",
        Description =
            @"Essa aplicação contém artefatos de software utilizados durante as
aulas da disciplina TÓPICOS ESPECIAIS EM DESENVOLVIMENTO DE SOFTWARE 1 -
Turma: 01 (2022.2), do INSTITUTO METROPOLE DIGITAL (IMD) / UFRN. 
Nesse sentido, os códigos e aplicações da forma como estão aqui registrados 
devem ser considerados como material utilizado para fins didáticos. 
Portanto, não representam uma sugestão ou insumo que deva ser aplicada para soluções reais.",
        TermsOfService = new Uri("https://creativecommons.org/licenses/by-nc/4.0/"),
        Contact = new OpenApiContact()
        {
            Name = "Yan Justino",
            Email = "contato@yanjustino.com",
            Url = new Uri("https://medium.com/yanjustino")
        },
        Extensions = new Dictionary<string, IOpenApiExtension>
        {
            { "x-reference", new OpenApiString("https://andersbrownworth.com/blockchain/blockchain") },
        },
        License = new OpenApiLicense
        {
            Name = "CC BY",
            Url = new Uri("https://creativecommons.org/licenses/by-nc/4.0/")
        }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    opt.IncludeXmlComments(xmlPath);
});

builder.Services.AddSingleton<IWalletRepository, WalletMemoryRepository>();
//builder.Services.AddSingleton<IWalletRepository, WalletDocumentDbRepository>();
builder.Services.AddSingleton<Blockchain>();

#endregion

#region Configure pipeline

var app = builder.Build();
app.UseSwagger();
app.UseSwaggerUI();
app.UseAuthorization();
app.MapControllers();
app.UseAllElasticApm(builder.Configuration);
app.Run();

#endregion

#region Configure Log
void ConfigureLogging()
{
    var enviromentVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
    
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile(
            $"appsettings.{enviromentVar}.json",
            optional: true)
        .Build();

    Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithExceptionDetails()
        .WriteTo.Debug()
        .WriteTo.Console()
        .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, enviromentVar))
        .Enrich.WithProperty("Environment", enviromentVar)
        .ReadFrom.Configuration(configuration)
        .CreateLogger();
}

ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
{
    var indexFormat = $"dotchain-{DateTime.UtcNow:yyyy-MM}";
    
    return new ElasticsearchSinkOptions(new Uri(configuration["ElasticConfiguration:Uri"]))
    {
        AutoRegisterTemplate = true,
        IndexFormat = indexFormat
    };
}

#endregion